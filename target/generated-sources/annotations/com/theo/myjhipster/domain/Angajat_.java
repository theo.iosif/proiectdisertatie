package com.theo.myjhipster.domain;

import java.time.Instant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Angajat.class)
public abstract class Angajat_ {

	public static volatile SingularAttribute<Angajat, String> nrTelefon;
	public static volatile SingularAttribute<Angajat, Angajat> manager;
	public static volatile SingularAttribute<Angajat, String> prenume;
	public static volatile SetAttribute<Angajat, Functie> functies;
	public static volatile SingularAttribute<Angajat, Long> salariu;
	public static volatile SingularAttribute<Angajat, Long> id;
	public static volatile SingularAttribute<Angajat, String> nume;
	public static volatile SingularAttribute<Angajat, Instant> dataAngajare;
	public static volatile SingularAttribute<Angajat, String> email;
	public static volatile SingularAttribute<Angajat, Departament> departament;

}

