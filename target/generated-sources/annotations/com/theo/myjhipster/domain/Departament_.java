package com.theo.myjhipster.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Departament.class)
public abstract class Departament_ {

	public static volatile SetAttribute<Departament, Angajat> angajats;
	public static volatile SingularAttribute<Departament, Long> id;
	public static volatile SingularAttribute<Departament, String> numeDepartament;

}

