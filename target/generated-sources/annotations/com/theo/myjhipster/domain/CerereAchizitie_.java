package com.theo.myjhipster.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CerereAchizitie.class)
public abstract class CerereAchizitie_ {

	public static volatile SingularAttribute<CerereAchizitie, String> numarProduse;
	public static volatile SingularAttribute<CerereAchizitie, String> prenumeManager;
	public static volatile SingularAttribute<CerereAchizitie, String> produs;
	public static volatile SingularAttribute<CerereAchizitie, String> prenumeSolicitant;
	public static volatile SingularAttribute<CerereAchizitie, String> numeDepartament;
	public static volatile SingularAttribute<CerereAchizitie, String> descriereProdus;
	public static volatile SingularAttribute<CerereAchizitie, String> categorieProdus;
	public static volatile SingularAttribute<CerereAchizitie, String> numeSolicitant;
	public static volatile SingularAttribute<CerereAchizitie, Long> id;
	public static volatile SingularAttribute<CerereAchizitie, Angajat> angajat;
	public static volatile SingularAttribute<CerereAchizitie, User> user;
	public static volatile SingularAttribute<CerereAchizitie, String> numeManager;
	public static volatile SingularAttribute<CerereAchizitie, Departament> departament;

}

