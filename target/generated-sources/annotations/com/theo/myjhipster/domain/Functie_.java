package com.theo.myjhipster.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Functie.class)
public abstract class Functie_ {

	public static volatile SingularAttribute<Functie, Long> salariuMax;
	public static volatile SingularAttribute<Functie, String> numeFunctie;
	public static volatile SingularAttribute<Functie, Long> salariuMin;
	public static volatile SingularAttribute<Functie, Long> id;
	public static volatile SingularAttribute<Functie, Angajat> angajat;

}

